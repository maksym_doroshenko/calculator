package com.agileengine.calculator.server;

import com.agileengine.calculator.Utils;
import com.agileengine.calculator.server.operators.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by maksym on 09.05.16.
 */
public class CalculatorServer extends Thread {

    private ServerSocket serverSocket;
    private static Map<String, Operator> commandsMap;

    static {
        commandsMap = new HashMap<>();
        commandsMap.put(PlusOperator.NAME, new PlusOperator());
        commandsMap.put(MinusOperator.NAME, new MinusOperator());
        commandsMap.put(TimesOperator.NAME, new TimesOperator());
        commandsMap.put(DivideOperator.NAME, new DivideOperator());
        commandsMap.put(OkOperator.NAME, new OkOperator());
    }

    public CalculatorServer(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    public void run() {
        while (true) {
            try {
                Socket client = serverSocket.accept();
                String[] args = readArgumentsFromClient(client);
                String operator = args[0];
                String[] numbers = Arrays.copyOfRange(args, 1, args.length);
                String result = process(operator, numbers);
                sendResponse(client, result);
                if (result.equals(Utils.CLOSE_RESPONSE)) {
                    client.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String[] readArgumentsFromClient(Socket socket) throws IOException {
        DataInputStream inputFromClient = new DataInputStream(socket.getInputStream());
        String[] arguments = new String[inputFromClient.readInt()];
        for (int i = 0; i < arguments.length; i++) {
            arguments[i] = inputFromClient.readUTF();
        }
        return arguments;
    }

    private String process(String operator, String[] arguments) {
        Operator command = commandsMap.get(operator);
        return command.execute(arguments);
    }

    private void sendResponse(Socket socket, String result) throws IOException {
        DataOutputStream out = new DataOutputStream(socket.getOutputStream());
        out.writeUTF(result);
    }


    public static void main(String[] args) {
        try {
            Properties properties = Utils.getProperties();
            int port = Integer.parseInt(properties.getProperty("port"));
            Thread serverThread = new CalculatorServer(port);
            serverThread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
