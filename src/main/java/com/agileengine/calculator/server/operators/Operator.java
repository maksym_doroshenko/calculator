package com.agileengine.calculator.server.operators;

/**
 * Created by maksym on 09.05.16.
 */
public interface Operator {
    String execute(String[] arg);
}
