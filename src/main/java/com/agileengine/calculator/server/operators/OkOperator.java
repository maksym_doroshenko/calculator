package com.agileengine.calculator.server.operators;

import com.agileengine.calculator.Utils;

/**
 * Created by maksym on 09.05.16.
 */
public class OkOperator implements Operator {

    public static final String NAME = "OK";

    @Override
    public String execute(String[] arg) {
        String result;
        if (arg.length == 1 && arg[0].equals(Utils.CLOSE_ARG_NAME)) {
            result = Utils.CLOSE_RESPONSE;
        } else {
            result = Utils.ERROR_RESPONSE;
        }
        return result;
    }
}
