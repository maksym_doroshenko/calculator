package com.agileengine.calculator.server.operators;

import com.agileengine.calculator.Utils;

/**
 * Created by maksym on 09.05.16.
 */
public class DivideOperator implements Operator {

    public static final String NAME = "DIVIDE";

    @Override
    public String execute(String[] arg) {
        try {
            int[] numbers = Utils.parseStrings(arg);
            int calculationResult = numbers[0];
            for (int i = 1; i < numbers.length; i++) {
                calculationResult /= numbers[i];
            }
            String result = Utils.SUCCESS_PREFIX + calculationResult;
            return result;
        } catch (Exception e) {
            return Utils.ERROR_RESPONSE;
        }
    }
}
