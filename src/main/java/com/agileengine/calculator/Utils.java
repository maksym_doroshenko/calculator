package com.agileengine.calculator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;

/**
 * Created by maksym on 09.05.16.
 */
public class Utils {
    public static final String SUCCESS_PREFIX = "RESULT ";
    public static final String ERROR_RESPONSE = "ERROR";
    public static final String CLOSE_RESPONSE = "CLOSED";
    public static final String CLOSE_ARG_NAME = "CLOSE";
    private static final String PROPERTIES_FILE_NAME = "calculator.properties";

    public static Properties getProperties() throws IOException {
        Properties properties = new Properties();
        InputStream inputStream = Utils.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME);
        if (inputStream != null) {
            properties.load(inputStream);
        } else {
            throw new FileNotFoundException("Property file '" + PROPERTIES_FILE_NAME + "' not found in the classpath");
        }
        return properties;
    }

    public static int[] parseStrings(String[] strings) {
        int[] arrayOfInts = Arrays.stream(strings).mapToInt(Integer::parseInt).toArray();
        for (int number : arrayOfInts) {
            if (number < 0) {
                throw new IllegalArgumentException("Not positive argument");
            }
        }
        return arrayOfInts;
    }
}
