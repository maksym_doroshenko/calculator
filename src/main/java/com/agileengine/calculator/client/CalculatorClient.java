package com.agileengine.calculator.client;

import com.agileengine.calculator.Utils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Properties;
import java.util.Scanner;

/**
 * Created by maksym on 09.05.16.
 */
public class CalculatorClient {

    private Socket socket;

    public CalculatorClient() throws IOException {
        Properties properties = Utils.getProperties();
        String host = properties.getProperty("host");
        int port = Integer.parseInt(properties.getProperty("port"));
        this.socket = new Socket(host, port);
    }

    private String[] readArgumentsFromConsole() {
        Scanner scanIn = new Scanner(System.in);
        String input = scanIn.nextLine();
        return input.split("\\s+");
    }

    private void sendArgumentsToServer(String[] args) throws IOException {
        DataOutputStream outToServer = new DataOutputStream(socket.getOutputStream());
        outToServer.writeInt(args.length);
        for (String arg : args) {
            outToServer.writeUTF(arg);
        }
    }

    private String readResponse() throws IOException {
        DataInputStream inFromServer = new DataInputStream(socket.getInputStream());
        return inFromServer.readUTF();
    }


    public static void main(String[] args) {
        while (true) {
            try {
                CalculatorClient client = new CalculatorClient();
                System.out.printf("Enter command please: ");
                String[] arguments = client.readArgumentsFromConsole();
                client.sendArgumentsToServer(arguments);
                String result = client.readResponse();
                System.out.println(result);
                if (result.equals(Utils.CLOSE_RESPONSE)) {
                    client.socket.close();
                    return;
                }
            } catch (IOException e) {
                e.getMessage();
            }
        }
    }
}
