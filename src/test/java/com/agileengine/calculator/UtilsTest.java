package com.agileengine.calculator;

import org.junit.Test;

import java.io.IOException;
import java.util.Properties;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by maksym on 10.05.16.
 */
public class UtilsTest {
    @Test
    public void testGetProperties() {
        Properties expected = new Properties();
        expected.put("host", "localhost");
        expected.put("port", "2518");
        try {
            assertEquals(expected, Utils.getProperties());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParseStrings() {
        int[] expected = {1, 2, 3};
        int[] actual = Utils.parseStrings(new String[]{"1", "2", "3"});
        assertArrayEquals(expected, actual);
    }
}
