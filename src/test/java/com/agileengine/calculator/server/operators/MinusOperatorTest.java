package com.agileengine.calculator.server.operators;

import com.agileengine.calculator.Utils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by maksym on 09.05.16.
 */
public class MinusOperatorTest {
    @Test
    public void testExecute() {
        MinusOperator operator = new MinusOperator();
        String result = Utils.SUCCESS_PREFIX + 2;
        assertEquals(result, operator.execute(new String[]{"5", "3"}));
    }

    @Test
    public void testExecuteError() {
        MinusOperator operator = new MinusOperator();
        String result = Utils.ERROR_RESPONSE;
        assertEquals(result, operator.execute(new String[]{"10", "i"}));
    }
}
