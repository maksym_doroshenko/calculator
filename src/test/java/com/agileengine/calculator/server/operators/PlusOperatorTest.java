package com.agileengine.calculator.server.operators;

import com.agileengine.calculator.Utils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by maksym on 09.05.16.
 */
public class PlusOperatorTest {
    @Test
    public void testExecute() {
        PlusOperator operator = new PlusOperator();
        String result = Utils.SUCCESS_PREFIX + 30;
        assertEquals(result, operator.execute(new String[]{"15", "5", "10", "0"}));
    }

    @Test
    public void testExecuteError() {
        PlusOperator operator = new PlusOperator();
        String result = Utils.ERROR_RESPONSE;
        assertEquals(result, operator.execute(new String[]{"10", "i"}));
    }
}
