package com.agileengine.calculator.server.operators;

import com.agileengine.calculator.Utils;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Created by maksym on 09.05.16.
 */
public class DivideOperatorTest {
    @Test
    public void testExecute() {
        DivideOperator operator = new DivideOperator();
        String result = Utils.SUCCESS_PREFIX + 2;
        assertEquals(result, operator.execute(new String[]{"10", "5"}));
    }

    @Test
    public void testExecuteError() {
        DivideOperator operator = new DivideOperator();
        String result = Utils.ERROR_RESPONSE;
        assertEquals(result, operator.execute(new String[]{"10", "0"}));
    }
}
