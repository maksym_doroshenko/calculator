package com.agileengine.calculator.server.operators;

import com.agileengine.calculator.Utils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by maksym on 09.05.16.
 */
public class TimesOperatorTest {
    @Test
    public void testExecute() {
        TimesOperator operator = new TimesOperator();
        String result = Utils.SUCCESS_PREFIX + 21;
        assertEquals(result, operator.execute(new String[]{"3", "7"}));
    }

    @Test
    public void testExecuteError() {
        TimesOperator operator = new TimesOperator();
        String result = Utils.ERROR_RESPONSE;
        assertEquals(result, operator.execute(new String[]{"10", "i"}));
    }
}
