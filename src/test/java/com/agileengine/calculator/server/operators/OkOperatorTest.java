package com.agileengine.calculator.server.operators;

import com.agileengine.calculator.Utils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by maksym on 09.05.16.
 */
public class OkOperatorTest {
    @Test
    public void testExecute() {
        OkOperator operator = new OkOperator();
        String result = Utils.CLOSE_RESPONSE;
        assertEquals(result, operator.execute(new String[]{Utils.CLOSE_ARG_NAME}));
    }

    @Test
    public void testExecuteError() {
        OkOperator operator = new OkOperator();
        String result = Utils.ERROR_RESPONSE;
        assertEquals(result, operator.execute(new String[]{"CLS"}));
    }
}
